# Shields

Code coverage shields for Bitbucket.

| Lcov                           | Tolerance        |
|--------------------------------|------------------|
| ![Alt text](./readme/ok.png)   | Green above 85%  | 
| ![Alt text](./readme/warn.png) | Orange 75% - 84% |
| ![Alt text](./readme/ko.png)   | Red below 75%    |

## Install

```
npm install @glint-ui/shields --save-dev
```

## Usage

Default path is `./coverage/lcov.info` which creates `coverage.png`.

```
shields [path]
```

Add the shield [image](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html#Markdownsyntaxguide-Images) to your README.md.

```
![Alt text](./coverage.png)
```