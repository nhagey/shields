#!/usr/bin/env node
'use strict'

// Program Dependencies
const fs = require('pn/fs')
const rfs = require('fs').readFileSync
const cp = require('child_process')
const assign = require('deep-assign');
const svg2png = require('svg2png')
const lcov2badge = require('lcov2badge')


// Program Defaults
const defaults = JSON.parse(rfs(`${__dirname}/shields-default.json`, 'utf8'))
const tempFile = '.tmp.shields'

// Optional Config
const configArg = process.argv[2]
let config
if (configArg) {
    try {
        const configTxt = rfs(`${configArg}`, 'utf8');
        config = JSON.parse(configTxt)
        config = assign({}, defaults, config)
    } catch (e) {
        config = assign({}, defaults, {
            lcov: {
                filePath: configArg
            }
        })
    }
}

// Program
lcov2badge.badge(config.lcov, function(err, svgBadge){
    if (err) {
        throw err;
    }
    
    fs.writeFileSync(tempFile, svgBadge, 'utf8')
    
    fs.readFile(tempFile)
        .then(svg2png)
        .then(buffer => fs.writeFile(config.lcov.outPath, buffer))
        .then(() => fs.unlink(tempFile))
        .then(() => console.log(`Shields added ${config.lcov.outPath}`))
        .catch(e => console.error(e));
});
